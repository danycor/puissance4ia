<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">

        <title>Puissance 4 IA</title>
        <meta name="description" content="Puissance 4 IA">
        <meta name="author" content="Dany CORBINEAU">

    </head>
    <style>
        *{
            border-collapse: collapse;
        }
        td{
            border:  1px solid #555;
            width: 10vh;
            height: 10vh;
        }

        .red{
            background-color: #f99;
        }
        .green{
            background-color: #ff9;
        }

    </style>
    <body>
        <form action="" method="POST">
            <input type="submit" name="reset" value="Reset"/>
            <table>
                <tr>
                    <th><?php if(collumnIsValide(1,$table) && $session->get('user_acual')!=0){ echo'<input type="submit" name="x" value="1"/>';}?></th>
                    <th><?php if(collumnIsValide(2,$table) && $session->get('user_acual')!=0){ echo'<input type="submit" name="x" value="2"/>';}?></th>
                    <th><?php if(collumnIsValide(3,$table) && $session->get('user_acual')!=0){ echo'<input type="submit" name="x" value="3"/>';}?></th>
                    <th><?php if(collumnIsValide(4,$table) && $session->get('user_acual')!=0){ echo'<input type="submit" name="x" value="4"/>';}?></th>
                    <th><?php if(collumnIsValide(5,$table) && $session->get('user_acual')!=0){ echo'<input type="submit" name="x" value="5"/>';}?></th>
                    <th><?php if(collumnIsValide(6,$table) && $session->get('user_acual')!=0){ echo'<input type="submit" name="x" value="6"/>';}?></th>
                    <th><?php if(collumnIsValide(7,$table) && $session->get('user_acual')!=0){ echo'<input type="submit" name="x" value="7"/>';}?></th>
                </tr>
                <?php
                    for($y=1;$y<=7;$y++){
                        echo '<tr>';
                        for($x=1;$x<=7;$x++){
                            $class_css='';
                            foreach($table as $cel){
                                if($cel['x']==$x && $cel['y']==$y){
                                    switch($cel['user']){
                                        case 1: $class_css='red'; break;
                                        case 2: $class_css='green'; break;
                                    }
                                }
                            }
                            echo '<td class="'.$class_css.'"></td>';
                        }
                    echo '</tr>';
                    }
                ?>
            </table>
        </form>
    </body>
</html>