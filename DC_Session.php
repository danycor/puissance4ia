<?php
/*
   Create By Dany CORBINEAU
   Project: c:\wamp64\www\ITRock\Lib
   File: DC_Session.php
   The $(date)
*/
class DC_Session
{
    public function __construct(){
         if(!isset($_SESSION)){
            session_start();
            $_SESSION['Start']=true;
         }
    }


    /**
     * get value of session var
     */
    public function get($name){
        if(isset($_SESSION) && isset($_SESSION[$name]))
            return $_SESSION[$name];
        else
            return null;
    }


    /**
     * Set a value to a session var
     */
    public function set($name,$value){
        $_SESSION[$name]=$value;
    }


    /**
     * End of the session
     */
    public function session_destroy(){
        session_destroy();
    }

    


}


?>