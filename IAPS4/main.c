#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct Datas Datas;
struct Datas{
    int profondeur;
    int id_ia;
    int id_player;
    int **table;
    int coup;
};

Datas loadDatas(int argc, char **argv);
int columnIsValide(int x,int **p);
int **copyTable(int **table);
int **IAPlay(int **table,int value,int id);
int IAMin(int **table, int profondeur, int iaId, int playerId);
int IAMax(int **table, int profondeur, int iaId, int playerId);
int testVictoryPoint(int **table, int x, int y, int mx, int my);
int testVictoire(int **plateau);
int IAEval(int **table,int profondeur, int iaId,int playerId);
int IAChoixCoup(int **table, int iaId, int playerId, int profondeur, int coup);
void freeTable(int **table);
int testAlignePoint(int **table, int x, int y, int mx, int my, int prof);
int IAAvangageAlignement(int **tableau, int iaId, int playerId, int nbAlignement);
float getPlacementCenterScore(int **table, int ia_id, int player_id);
Datas *copyData(Datas *d,int coup);
void *runThread(void *datas);
int IAThreadRun(Datas *d);


int main(int argc, char **argv)
{
    Datas d = loadDatas(argc, argv);

   /* pthread_create (&thread1,NULL,write,&a);
    pthread_create (&thread2,NULL,write,&c);
    pthread_create (&thread3,NULL,write,&b);


    pthread_join(thread1,NULL);
    pthread_join(thread2,NULL);
    pthread_join(thread3,NULL);*/

    int score = IAThreadRun(&d);
    printf("%d\n",score);
    freeTable(d.table);
    return 0;
}

int IAThreadRun(Datas *d){
    int values[7]={0}, j;
    pthread_t thread[7];
    int maxVal=-9999999;
    int meilleurCoup=1;
    Datas *datas[7];

    for(j=0;j<7;++j){
        datas[j]=copyData(d,j);
    }

    for(j=0;j<7;++j){
        if(columnIsValide(j+1,d->table)){
            pthread_create(&thread[j],NULL,runThread,(void*)datas[j]);
        }
    }
    for(j=0;j<7;++j){
        if(columnIsValide(j+1,d->table)){
            pthread_join(thread[j],&(values[j]));
            //printf("%d - %d\n",j+1,values[j]);
            if(values[j]>maxVal){
                maxVal=values[j];
                meilleurCoup=j+1;
            }
        }
    }

    for(j=0;j<7;++j){
        freeTable(datas[j]->table);
    }

    return meilleurCoup;
}

Datas loadDatas(int argc, char **argv){
    Datas d={0};

    d.table = (int**)malloc(sizeof(int*)*7);
    int j,k;
    for(j=0;j<7;++j){
        d.table[j]=(int*)malloc(sizeof(int)*7);
        for(k=0;k<7;++k){
            d.table[j][k]=0;
        }
    }

    int argC=1;
    for(j=0;j<7;++j){
        for(k=0;k<7;++k){
            d.table[j][k]= atoi(argv[argC]);
            ++argC;
        }
    }
    d.profondeur = atoi(argv[argC]);
    ++argC;
    d.id_ia = atoi(argv[argC]);
    ++argC;
    d.id_player = atoi(argv[argC]);


    return d;
}

void *runThread(void *datas){
    Datas *d=datas;
    int score=IAChoixCoup(d->table,d->id_ia,d->id_player,d->profondeur,d->coup);
    return (void*)score;
    return NULL;
}

int IAChoixCoup(int **table, int iaId, int playerId, int profondeur, int coup){
    int **tableT = IAPlay(table,coup,iaId);
    int minVal = IAMin(tableT, profondeur, iaId, playerId);
    free(tableT);
    return minVal;
}

int columnIsValide(int x,int **p){
    if(p[0][x-1]==0)
        return 1;
    return 0;
}

int **IAPlay(int **table,int value,int id){
    int i;
    int **retTable=copyTable(table);

    if(columnIsValide(value,retTable)){
        for(i=6;i>=0;--i){
            if(retTable[i][value-1]==0){
                retTable[i][value-1]=id;
                break;
            }
        }
    }
    return retTable;
}

int **copyTable(int **table){
    int **retTable = (int**)malloc(sizeof(int*)*7);
    int j,k;
    for(j=0;j<7;++j){
        retTable[j]=(int*)malloc(sizeof(int)*7);
        for(k=0;k<7;++k){
            retTable[j][k]=table[j][k];
        }
    }
    return retTable;
}

int IAMin(int **table, int profondeur, int iaId, int playerId){
    if(profondeur<=0 || testVictoire(table)!=0){
        return IAEval(table, profondeur, iaId, playerId);
    }
    else{
        int i;
        int minVal=9999999;
        for(i=1; i<=7; ++i){
            if(columnIsValide(i,table)){
                int **tableT=IAPlay(table,i,playerId);
                int maxVal=IAMax(tableT, profondeur-1, iaId, playerId);
                freeTable(tableT);
                if(maxVal<minVal){
                    minVal=maxVal;
                }
            }
        }
        return minVal;
    }
}

int IAMax(int **table, int profondeur, int iaId, int playerId){
    if(profondeur<=0 || testVictoire(table)!=0){
        return IAEval(table, profondeur, iaId, playerId);
    }
    else{
        int maxVal=-9999999;
        int i;
        for(i=1; i<=7; ++i){
            if(columnIsValide(i,table)){
                int **tableT=IAPlay(table,i,iaId);
                int minVal=IAMin(tableT, profondeur-1, iaId, playerId);
                freeTable(tableT);
                if(minVal>maxVal){
                    maxVal=minVal;
                }
            }
        }
        return maxVal;
    }
}

int IAEval(int **table,int profondeur, int iaId,int playerId){

    float score = profondeur*80;

    int victoire = testVictoire(table);
    if(victoire == iaId)
        score+=1000;
    else if(victoire == playerId)
        score-=1000;

    score+=IAAvangageAlignement(table, iaId, playerId,2)*1;
    score+=IAAvangageAlignement(table, iaId, playerId,3)*3;
    score+=getPlacementCenterScore(table, iaId, playerId);
    return score;
}


int testVictoire(int **plateau){
    int player_win=0;
    int x,y;
    for(y=0;y<7;++y)
    {
        for(x=0;x<7;++x){
            if(plateau[y][x]!=0){
                if(testVictoryPoint(plateau,x,y,1,0)){return plateau[y][x];}
                if(testVictoryPoint(plateau,x,y,0,1)){return plateau[y][x];}
                if(testVictoryPoint(plateau,x,y,1,1)){return plateau[y][x];}
                if(testVictoryPoint(plateau,x,y,1,-1)){return plateau[y][x];}
            }
        }
    }
    return player_win;
}

int testVictoryPoint(int **table, int x, int y, int mx, int my){
    if(x+(3*mx)>=0 && x+(3*mx)<=6 && y+(3*my)>=0 && y+(3*my)<=6){
        int i;
        int cell_values[4]={0};
        int cellPos=0;
        for(i=0;i<4;++i){
            cell_values[cellPos]=table[y+(i*my)][x+(i*mx)];
            ++cellPos;
            if(table[y+(i*my)][x+(i*mx)]==0){
                return 0;
            }
        }
        if(cell_values[0]==cell_values[1] && cell_values[1]==cell_values[2] && cell_values[2]==cell_values[3]){
            return 1;
        }
    }
    return 0;
}


int IAAvangageAlignement(int **tableau, int iaId, int playerId, int nbAlignement){
    int nb_alignement=0;
    int x,y;
    for(y=0;y<7;++y){
        for(x=0;x<7;++x){
            if(tableau[y][x]==iaId){
                nb_alignement+=testAlignePoint(tableau, x,y, 1,0,nbAlignement);
                nb_alignement+=testAlignePoint(tableau, x,y, 1,1,nbAlignement);
                nb_alignement+=testAlignePoint(tableau, x,y, 0,1,nbAlignement);
                nb_alignement+=testAlignePoint(tableau, x,y, 1,-1,nbAlignement);
            }else if(tableau[y][x]==playerId){
                nb_alignement-=testAlignePoint(tableau, x,y, 1,0,nbAlignement)*2;
                nb_alignement-=testAlignePoint(tableau, x,y, 1,1,nbAlignement)*2;
                nb_alignement-=testAlignePoint(tableau, x,y, 0,1,nbAlignement)*2;
                nb_alignement-=testAlignePoint(tableau, x,y, 1,-1,nbAlignement)*2;
            }
        }
    }
    return nb_alignement;
}

int testAlignePoint(int **table, int x, int y, int mx, int my, int prof){
    if(x+(3*mx)>=0 && x+(3*mx)<=6 && y+(3*my)>=0 && y+(3*my)<=6){
        int cell_values[prof];
        int i;
        for(i=0;i<prof;++i){
            cell_values[i]=table[y+(i*my)][x+(i*mx)];
        }
        for(i=0;i<prof-1;++i){
            if(cell_values[i] != cell_values[i+1])
                return 0;
        }
        return 1;
    }
    return 0;
}

void freeTable(int **table){
    int j;
    for(j=0;j<7;++j){
        free(table[j]);
    }
    free(table);
}

float getPlacementCenterScore(int **table, int ia_id, int player_id){
    float sc_ia = 0;
    float sc_player = 0;
    int y,x;
    for(y=0;y<7;++y){
        for(x=0;x<7;++x){
            if(table[y][x] == ia_id){
                sc_ia += ( 4./ (abs(x-3.)+1.) ) * 5.;
            } else  if(table[y][x] == player_id) {
                sc_player += (4./(abs(x-3.)+1.) )*5.;
            }
        }
    }
    return sc_ia - sc_player;
}

Datas *copyData(Datas *d,int coup){
    Datas *dc=(Datas*)malloc(sizeof(Datas));
    dc->id_ia=d->id_ia;
    dc->id_player=d->id_player;
    dc->profondeur=d->profondeur;
    dc->table=copyTable(d->table);
    dc->coup=coup+1;
    return dc;
}

