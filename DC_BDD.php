<?php
/*
   Create By Dany CORBINEAU
   Project: c:\wamp64\www\ITRock\Lib
   File: DC_BDD.php
   The $(date)
*/
class DC_BDD 
{
    public $userName; // bdd user name
    public $bddName; // name of the bdd
    public $password; // password of bdd user

    private $bdd; // PDO bdd


    /**
     * Init connexion's variables
     */
    public function initialisation($user,$password,$bddName){
        $this->userName=$user;
        $this->password=$password;
        $this->bddName=$bddName;
    } 

    /**
     * Connect to Bdd / generate exception if fail
     */
    public function connexion(){
        $this->bdd = new PDO('mysql:host=localhost;dbname='.$this->bddName.';charset=utf8', $this->userName,$this->password);
    }

    /**
     * Send request to get data to the Bdd / Return datas of the request
     * $resquest : sql request with :VName for data
     * $DatasArray : datas use in request define with [ ':VName' => 456 ]
     */
    public function execRequest($request, $datasArray=null){
        $prepareRequest = $this->bdd->prepare($request);
        $prepareRequest->execute($datasArray);
        $datas = $prepareRequest->fetchAll();
        $prepareRequest->closeCursor();
        // var_dump($datas);
        // var_dump($datasArray);
        return $datas;
    }


    /**
     * Destructor
     */
    function __destruct() {
        $this->userName=null;
        $this->password=null;
        $this->bddName=null;
        $this->bdd=null;
    }

}



?>