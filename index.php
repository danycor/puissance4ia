<?php
include "DC_BDD.php";
include "DC_Form.php";
include "DC_Session.php";

$session = new DC_Session();

function fill_table($bdd){
    for ($x = 1; $x <= 7; $x++) {
        for ($y = 1; $y <= 7; $y++) {
            $bdd->execRequest("
            insert into current_cell(id,x,y,user)
            values (null,$x,$y,0); ");
        }
    }
}

function add_pion(&$table, $bdd, $value, $player_id){
    $pion_add=false;
    $can_play=true;
    $pos_y=0;

    for($y=7;$y>=1 && !$pion_add; $y--){
        foreach($table as $cel){
            if($cel['x']==$value && $cel['y']==$y && $cel['user']==0){
                $pos_y=$y;
                $pion_add=true;
                break;
            }
        }
    }

    if($pos_y>0){
        foreach($table as $cel){
            if($cel['x']==$value && $cel['y']==$pos_y && $cel['user']==0){
                $cel['user']=$player_id;
                $bdd->execRequest("UPDATE `current_cell` SET `user` = ".$cel['user']." WHERE `current_cell`.`id` = ".$cel['id'].";");
            }
        }
    }
}

function reset_game($bdd){
    $bdd->execRequest("delete from current_cell");
    fill_table($bdd);
}

function collumnIsValide($x, $table){
    foreach($table as $cel){
        if($cel['x']==$x && $cel['y']==1){
            if($cel['user']!=0){
                return false;
            }
            else{
                return true;
            }
        }
    }
}

function testVictoire($table){
    $player_win=0;
    foreach($table as $cel){
        if($cel['user']!=0){
            if(testVictoryPoint($table,$cel['x'],$cel['y'],1,0)){return $cel['user'];}
            if(testVictoryPoint($table,$cel['x'],$cel['y'],0,1)){return $cel['user'];}
            if(testVictoryPoint($table,$cel['x'],$cel['y'],1,1)){return $cel['user'];}
            if(testVictoryPoint($table,$cel['x'],$cel['y'],1,-1)){return $cel['user'];}
        }
    }
    return $player_win;
}
function testVictoryPoint($table, $x, $y, $mx, $my){
    if($x+(3*$mx)>=1 && $x+(3*$mx)<=7 && $y+(3*$my)>=1 && $y+(3*$my)<=7){
        $cell_values=[];
        for($i=0;$i<4;$i++){
            foreach($table as $cel){
                if($cel['x']==($x+($mx*$i)) && $cel['y']==($y+($my*$i))){
                    $cell_values[]=$cel['user'];
                }
            }
        }
        if($cell_values[0]==$cell_values[1] && $cell_values[1]==$cell_values[2] && $cell_values[2]==$cell_values[3] && $cell_values[0]!=0){
            return true;
        }
    }
    return false;
}

function IAChoixCoup($table, $iaId, $playerId){
    $profondeur = 3;
    $meilleurCoup = rand(1,7);
    $maxVal=-9999999;
    for($i=1; $i<=7; $i++){
        if(collumnIsValide($i,$table)){
            $tableT=IAPlay($table,$i, $iaId);
            $minVal=IAMin($tableT,$profondeur, $iaId, $playerId);
            //$minVal=IAEval($tableT, $profondeur, $iaId, $playerId);
            if($minVal>$maxVal){
                $maxVal=$minVal;
                $meilleurCoup=$i;
            }
        }
    }
    return $meilleurCoup;
}

function IAPlay($table, $colN, $id){
    $pion_add=false;
    $can_play=true;
    $pos_y=0;

    for($y=7;$y>=1 && !$pion_add; $y--){
        foreach($table as $cel){
            if($cel['x']==$colN && $cel['y']==$y && $cel['user']==0){
                $pos_y=$y;
                $pion_add=true;
                break;
            }
        }
    }

    if($pos_y>0){
        foreach($table as $cel){
            if($cel['x']==$colN && $cel['y']==$pos_y && $cel['user']==0){
                $cel['user']=$id;
            }
        }
    }
    return $table;
}

function IAMin($table, $profondeur, $iaId, $playerId){
    if($profondeur<=0){
        IAEval($table, $profondeur, $iaId, $playerId);
    }
    else{
        $minVal=9999999;
        for($i=1; $i<=7; $i++){
            if(collumnIsValide($i,$table)){
                $tableT=IAPlay($table,$i,$playerId);
                $maxVal=IAMax($tableT, $profondeur-1, $iaId, $playerId);
                if($maxVal<$minVal){
                    $minVal=$maxVal;
                }
            }
        }
    }
}

function IAMax($table, $profondeur, $iaId, $playerId){
    if($profondeur<=0){
        IAEval($table, $profondeur, $iaId, $playerId);
    }
    else{
        $maxVal=-9999999;
        for($i=1; $i<=7; $i++){
            if(collumnIsValide($i,$table)){
                $tableT=IAPlay($table,$i,$iaId);
                $minVal=IAMin($tableT, $profondeur-1, $iaId, $playerId);
                if($minVal>$maxVal){
                    $maxVal=$minVal;
                }
            }
        }
    }
}

function IAEval($table, $profondeur, $iaId, $playerId){
    $score = -$profondeur;
    $victoire = testVictoire($table);
    if($victoire == $iaId)
        $score+=1000;
    else
        $score-=1000;
}

$formPost=new DC_Form();

try
{
    /**
     * Connexion
     */
    $bdd=new DC_BDD();
    $bdd->initialisation("root","","puissance4ia");
    $bdd->connexion();

    $table = $bdd->execRequest("select * from current_cell");
    if(count($table)>0){
        if($formPost->getVar('reset',"err")=="Reset"){
            reset_game($bdd);
            $session->set('user_acual',1);
            $table = $bdd->execRequest("select * from current_cell");
        } else if($formPost->getVar('x',0)!=0){
            echo $formPost->getVar('x',0);
            add_pion($table,$bdd,$formPost->getVar('x',0),$session->get('user_acual'));
            $session->set('user_acual',($session->get('user_acual')==1) ? 2 : 1);
            $table = $bdd->execRequest("select * from current_cell");
            $user_win = testVictoire($table);
            if( $user_win!=0){
                $actuel_user=$session->get('user_acual');
                $session->set('user_acual',0);
                echo '<br/>player:'.$actuel_user.' loose<br/>';
            } else {
                
                $ia_coup=0;

                $playerId=1;
                if($session->get('user_acual')==1)
                    $playerId=2;
                do{
                    $ia_coup = IAChoixCoup($table,$session->get('user_acual'),$playerId);
                }while(!collumnIsValide($ia_coup, $table));

                echo 'IA PLAY :'.$ia_coup;
                add_pion($table,$bdd,$ia_coup,$session->get('user_acual'));
                $session->set('user_acual',($session->get('user_acual')==1) ? 2 : 1);
                $table = $bdd->execRequest("select * from current_cell");
                $user_win = testVictoire($table);
                if( $user_win!=0){
                    $actuel_user=$session->get('user_acual');
                    $session->set('user_acual',0);
                    echo '<br/>player:'.$actuel_user.' loose<br/>';
                }
            }
        }
    } else {
        fill_table($bdd);
    }
    //var_dump($table);
    
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}

require "menu.php"

?>