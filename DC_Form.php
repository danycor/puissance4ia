<?php
/*
   Create By Dany CORBINEAU
   Project: c:\wamp64\www\ITRock\Lib
   File: DC_Form.php
   The $(date)
*/
class DC_Form{


    /**
     * Get secure data from form
     */
    public function getVar($vName,$notFindValue){
        if(isset($_POST) && isset($_POST[$vName]))
            return htmlspecialchars($_POST[$vName]);
        else
            return $notFindValue;
    }

    /**
     * Get file datas when file is send
     */
    public function getFileFormData($fName){
        if(isset($_FILES) && isset($_FILES[$fName]))
            return $_FILES[$fName];
        else
            return null;
    }

    /**
     * Save a sending file
     */
    public function transfertFileToDir($fName,$dirPath){
        if($_FILES[$fName]["error"]== UPLOAD_ERR_OK ){
            $tmp_name = $_FILES[$fName]["tmp_name"];
            $name = basename($_FILES[$fName]["name"]);
            move_uploaded_file($tmp_name, "$dirPath/$name");
        }
        else if($_FILES[$fName]["error"]==UPLOAD_ERR_FORM_SIZE){
            throw new Exception("File OverSize");
        }
        else if($_FILES[$fName]["error"]==UPLOAD_ERR_NO_FILE){
            throw new Exception("No file find");
        }
        else {
            throw new Exception("File error");
        }
    }


    /**
     * Test the size of file. Generate exception if file size is more of sizeMax in octet
     */
    public function testFileSize($fName, $sizeMax){
        if ($_FILES[$fName]['size'] >$sizeMax) {
            throw new Exception($fName.' filesize limit. ('.$_FILES[$fName]['size'].'>'.$sizeMax.')');
        }
    }

}

?>